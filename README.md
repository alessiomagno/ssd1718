# Elementi tecnici inclusi finora nel progetto SSD -> http://astarte.csr.unibo.it/miosito/

# -Architettura MVC, comunicazione via eventi
# -Amministrazione SQL Server, SqlLocalDb
# -Interrogazione SQL Server, sqlcmd
# -(opzionale) SQL Server MS
# -Db provider per SQLite
# -DataReader per SQLite e SQL Server
# -Stringa di connessione da App.config
# -DB factories
# -EF, creazione object model database first
# -Statistiche -> verificare che il campione è una distribuzione normale- Slide 65+)