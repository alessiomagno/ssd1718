﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using BackEnd;
using RDotNet;
using System.IO;

namespace WindowsFormsApp1
{
    class Model
    {
        public delegate void viewEventHandler(object sender, string textToWrite); // questo gestisce l'evento (mittente e testo generato per l'interfaccia)
        public event viewEventHandler FlushText;  // questo genera l'evento

        public void ConnectDotNetDB(string connString, bool isSQLLite, string idCliente)
        {

            IDbConnection conn = null;
            try
            {
                if (isSQLLite) conn = new SQLiteConnection(connString);
                else conn = new SqlConnection(connString);

                conn.Open();
                IDbCommand com = conn.CreateCommand();
                //string queryText = "select id,nome from clienti";
                string queryText = "SELECT idserie,periodo,val FROM histordini where idserie = @id";
                com.CommandText = queryText;

                IDbDataParameter param = com.CreateParameter();
                param.DbType = DbType.Int32;
                param.ParameterName = "@id";
                param.Value = Convert.ToInt32(idCliente);
                com.Parameters.Add(param);

                IDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    FlushText(this, reader["idserie"] + " " + reader["periodo"] + " " + reader["val"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                FlushText(this, e.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

        }

        public void ConnectWithFactory(string connString, bool isSQLLite, string idSerie)
        {
            DataSet ds = new DataSet();
            DbProviderFactory dbFactory = null;
            if (isSQLLite) dbFactory = DbProviderFactories.GetFactory("System.Data.SQLite.EF6"); 
            else dbFactory = DbProviderFactories.GetFactory("System.Data.SqlClient");
            using (DbConnection conn = dbFactory.CreateConnection())
                {
                    try
                    {
                        conn.ConnectionString = connString;
                        conn.Open();
                        DbDataAdapter dbAdapter = dbFactory.CreateDataAdapter();
                        DbCommand dbCommand = conn.CreateCommand();
                        dbCommand.CommandText = "SELECT idserie,periodo,val FROM histordini where idserie = @id";
                        IDbDataParameter param = dbCommand.CreateParameter();
                        param.DbType = DbType.Int32;
                        param.ParameterName = "@id";
                        param.Value = Convert.ToInt32(idSerie);
                        dbCommand.Parameters.Add(param);
                        dbAdapter.SelectCommand = dbCommand;
                        dbAdapter.Fill(ds);
                        ds.Tables[0].TableName = "histordini";
                        foreach (DataRow dr in ds.Tables["histordini"].Rows)
                            FlushText(this, dr["idserie"] + " " + dr["periodo"] + " " + dr["val"]);
                    }
                    catch (Exception ex)
                    {
                        FlushText(this, "[FillDataSet] Error: " + ex.Message + "-> Check the idSerie inserted!");
                    }
                    finally
                    {
                        if (conn.State == ConnectionState.Open) conn.Close();
                    }
                
            }
        }

        public List<histordiniEF> ReadViaEF(int idCli) 
        {
            List<histordiniEF> lstOrdini = new List<histordiniEF>();
            ordini2017Entities context = new ordini2017Entities();
           
            using (context = new ordini2017Entities())
            {
                
                lstOrdini = context.histordiniEF.SqlQuery("Select * from histordini where idserie=" + idCli).ToList();

            }
            return lstOrdini;
        }
        //PREVISIONE SERIE STORICHE

        public void ArimaForecastWithR(string idSerie)
        {
            createCSVFromDB(idSerie);
            if (Int32.Parse(idSerie) <= 34 && Int32.Parse(idSerie) >= 0)
            {
                REngine engine = REngine.GetInstance();

                string path = System.Environment.GetEnvironmentVariable("Path");
                path = @"C:/Users/Alessio/Documents/R/win-library/3.4/" + @"C:/Program Files/R/R-3.4.2/bin/x64" + path;
                System.Environment.SetEnvironmentVariable("Path", path);
                REngine.SetEnvironmentVariables();
                engine = REngine.GetInstance();
                engine.Initialize();
                engine.Evaluate("library(tseries)");
                engine.Evaluate("library(forecast)");
                engine.Evaluate("data <- read.csv(\"C:/Users/Alessio/Desktop/Esame di Sistemi di Supporto alle Decisioni/Previsione serie storiche/currentSerie.csv\")");
                engine.Evaluate("myts <- ts(data[,3], start = c(" + idSerie + ", 0), end = c(" + idSerie + ", 34))");
                engine.Evaluate("plot(myts, xlab='Month', ylab = 'Sales')");
                engine.Evaluate("arimafit<-auto.arima(myts, stepwise = FALSE, approximation = FALSE)");
                engine.Evaluate("plot(forecast(arimafit, h=25))");
                engine.Evaluate("firstP <- as.numeric(gsub('.*:', '', summary(forecast(arimafit, h = 3))[2,1]))");
                engine.Evaluate("secondP <- as.numeric(gsub('.*:', '', summary(forecast(arimafit, h = 3))[3,1]))");
                string[] firstPrediction = engine.Evaluate("sprintf(\" % .1f\",firstP)").AsCharacter().ToArray();
                string[] secondPrediction = engine.Evaluate("sprintf(\" % .1f\",secondP)").AsCharacter().ToArray();
                FlushText(this, "Coming soon values for set:" + idSerie);
                FlushText(this, "35th monthly -> " + firstPrediction[0]);
                FlushText(this, "36th monthly -> " + secondPrediction[0]);
                engine.ClearGlobalEnvironment();
            }
            else {
                FlushText(this,"Idserie have to be between 0 and 34");
            }
        }

        public void createCSVFromDB(string idSerie)
        {
            var csv = new StringBuilder();
            IDbConnection conn = null;
                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalDbConn"].ConnectionString);

                    conn.Open();
                    IDbCommand com = conn.CreateCommand();
                    string queryText = "SELECT idserie,periodo,val FROM histordini where idserie = @id";
                    com.CommandText = queryText;

                    IDbDataParameter param = com.CreateParameter();
                    param.DbType = DbType.Int32;
                    param.ParameterName = "@id";
                    param.Value = Convert.ToInt32(idSerie);
                    com.Parameters.Add(param);

                    IDataReader reader = com.ExecuteReader();
                    csv.AppendLine("ID,MONTH,SALE");
                    while (reader.Read())
                    {
                        var first = reader["idserie"].ToString();
                        var second = reader["periodo"].ToString();
                        var third = reader["val"].ToString();
                        var newLine = $"{first},{second},{third}";
                        csv.AppendLine(newLine);
                    }
                    reader.Close();
                }
                catch (Exception e)
                {
                    FlushText(this, e.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                
            }
            File.WriteAllText("C:/Users/Alessio/Desktop/Esame di Sistemi di Supporto alle Decisioni/Previsione serie storiche/currentSerie.csv", csv.ToString());


        }
    }
}

