﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class tbidcliente : Form
    {
        Controller C = new Controller();

        public tbidcliente()
        {
            InitializeComponent();
            C.FlushText += viewEventHandler; // associo il codice all'handler nella applogic
        }

        private void viewEventHandler(object sender, string textToWrite)
        {
            txtConsole.AppendText(textToWrite + Environment.NewLine);
        }

        

        private void btn_Connect(object sender, EventArgs e)
        {
            C.ConnectSimple(rbSQLite.Checked, tbidclienti.Text);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void rbSQLite_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void btnFactory_Click(object sender, EventArgs e)
        {
            C.ConnectWithFactory(rbSQLite.Checked, tbidclienti.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            C.ReadViaEF(rbSQLite.Checked, tbidclienti.Text);
        }

        private void btnEF_Click(object sender, EventArgs e)
        {
            C.ReadViaEF(rbSQLite.Checked, tbidclienti.Text);
        }

        private void arimaBtn_Click(object sender, EventArgs e)
        {
            C.ArimaForecast(idSerieTxt.Text);
        }

        private void textBox1_TextChanged_2(object sender, EventArgs e)
        {

        }

        private void txtConsole_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
