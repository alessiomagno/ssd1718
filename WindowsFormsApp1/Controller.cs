﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json;

namespace WindowsFormsApp1
{
    class Controller
    {
        Model M = new Model();

        public delegate void viewEventHandler(object sender, string textToWrite); // questo gestisce l'evento
        public event viewEventHandler FlushText;  // questo genera l'evento

        public Controller()
        {
            M.FlushText += controllerViewEventHandler; //Aggiungo un handler agli eventi del model 
        }

        private void controllerViewEventHandler(object sender, string textToWrite)
        {
            FlushText(this, textToWrite);
        }

         public void ConnectSimple(bool isSqlLite, string idCliente)
        {
            string sqLiteConnString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            string sqLServerConnString = ConfigurationManager.ConnectionStrings["LocalDbConn"].ConnectionString;
            if (isSqlLite) M.ConnectDotNetDB(sqLiteConnString, isSqlLite, idCliente);
            else M.ConnectDotNetDB(sqLServerConnString, false, idCliente);

        }
        public void ConnectWithFactory(bool isSqlLite, string idSerie)
        {
            string sqLiteConnString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            string sqLServerConnString = ConfigurationManager.ConnectionStrings["LocalDbConn"].ConnectionString;
            if (isSqlLite) M.ConnectWithFactory(sqLiteConnString, isSqlLite, idSerie);
            else M.ConnectWithFactory(sqLServerConnString, false, idSerie);
        }


        public void ReadViaEF(bool isSQLite, string idCli)
        {
            if (isSQLite)FlushText(this, "Database provider not supported for this version of VisualStudio 2017("+ Environment.Version.ToString()+")");
            else FlushText(this, JsonConvert.SerializeObject(M.ReadViaEF(Convert.ToInt32(idCli))));
            
        }

        public void ArimaForecast(string idSerie) {

        M.ArimaForecastWithR(idSerie);
        }
    }
}
