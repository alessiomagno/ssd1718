﻿namespace WindowsFormsApp1
{
    partial class tbidcliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtConsole = new System.Windows.Forms.TextBox();
            this.readDotNet = new System.Windows.Forms.Button();
            this.rbSQLite = new System.Windows.Forms.RadioButton();
            this.rbSQLServer = new System.Windows.Forms.RadioButton();
            this.gbServer = new System.Windows.Forms.GroupBox();
            this.idclientelbl = new System.Windows.Forms.Label();
            this.tbidclienti = new System.Windows.Forms.TextBox();
            this.btnFactory = new System.Windows.Forms.Button();
            this.btnEF = new System.Windows.Forms.Button();
            this.arimaBtn = new System.Windows.Forms.Button();
            this.idSerieTxt = new System.Windows.Forms.TextBox();
            this.idSerieLbl = new System.Windows.Forms.Label();
            this.gbServer.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtConsole
            // 
            this.txtConsole.Location = new System.Drawing.Point(214, 86);
            this.txtConsole.Multiline = true;
            this.txtConsole.Name = "txtConsole";
            this.txtConsole.Size = new System.Drawing.Size(338, 295);
            this.txtConsole.TabIndex = 1;
            this.txtConsole.TextChanged += new System.EventHandler(this.txtConsole_TextChanged);
            // 
            // readDotNet
            // 
            this.readDotNet.Location = new System.Drawing.Point(30, 90);
            this.readDotNet.Name = "readDotNet";
            this.readDotNet.Size = new System.Drawing.Size(168, 36);
            this.readDotNet.TabIndex = 2;
            this.readDotNet.Text = "Read via .NET DB";
            this.readDotNet.UseVisualStyleBackColor = true;
            this.readDotNet.Click += new System.EventHandler(this.btn_Connect);
            // 
            // rbSQLite
            // 
            this.rbSQLite.AutoSize = true;
            this.rbSQLite.Checked = true;
            this.rbSQLite.Location = new System.Drawing.Point(6, 19);
            this.rbSQLite.Name = "rbSQLite";
            this.rbSQLite.Size = new System.Drawing.Size(63, 17);
            this.rbSQLite.TabIndex = 3;
            this.rbSQLite.TabStop = true;
            this.rbSQLite.Text = "SQLLite";
            this.rbSQLite.UseVisualStyleBackColor = true;
            this.rbSQLite.CheckedChanged += new System.EventHandler(this.rbSQLite_CheckedChanged);
            // 
            // rbSQLServer
            // 
            this.rbSQLServer.AutoSize = true;
            this.rbSQLServer.Location = new System.Drawing.Point(90, 19);
            this.rbSQLServer.Name = "rbSQLServer";
            this.rbSQLServer.Size = new System.Drawing.Size(80, 17);
            this.rbSQLServer.TabIndex = 4;
            this.rbSQLServer.Text = "SQL Server";
            this.rbSQLServer.UseVisualStyleBackColor = true;
            // 
            // gbServer
            // 
            this.gbServer.Controls.Add(this.rbSQLite);
            this.gbServer.Controls.Add(this.rbSQLServer);
            this.gbServer.Location = new System.Drawing.Point(214, 26);
            this.gbServer.Name = "gbServer";
            this.gbServer.Size = new System.Drawing.Size(176, 53);
            this.gbServer.TabIndex = 5;
            this.gbServer.TabStop = false;
            this.gbServer.Text = "DBMS";
            // 
            // idclientelbl
            // 
            this.idclientelbl.AutoSize = true;
            this.idclientelbl.Location = new System.Drawing.Point(30, 48);
            this.idclientelbl.Name = "idclientelbl";
            this.idclientelbl.Size = new System.Drawing.Size(53, 13);
            this.idclientelbl.TabIndex = 6;
            this.idclientelbl.Text = "ID Cliente";
            // 
            // tbidclienti
            // 
            this.tbidclienti.Location = new System.Drawing.Point(98, 45);
            this.tbidclienti.Name = "tbidclienti";
            this.tbidclienti.Size = new System.Drawing.Size(100, 20);
            this.tbidclienti.TabIndex = 8;
            this.tbidclienti.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // btnFactory
            // 
            this.btnFactory.Location = new System.Drawing.Point(30, 132);
            this.btnFactory.Name = "btnFactory";
            this.btnFactory.Size = new System.Drawing.Size(165, 33);
            this.btnFactory.TabIndex = 9;
            this.btnFactory.Text = "Read via Factory";
            this.btnFactory.UseVisualStyleBackColor = true;
            this.btnFactory.Click += new System.EventHandler(this.btnFactory_Click);
            // 
            // btnEF
            // 
            this.btnEF.Location = new System.Drawing.Point(30, 171);
            this.btnEF.Name = "btnEF";
            this.btnEF.Size = new System.Drawing.Size(165, 33);
            this.btnEF.TabIndex = 10;
            this.btnEF.Text = "Read via EF";
            this.btnEF.UseVisualStyleBackColor = true;
            this.btnEF.Click += new System.EventHandler(this.btnEF_Click);
            // 
            // arimaBtn
            // 
            this.arimaBtn.Location = new System.Drawing.Point(30, 240);
            this.arimaBtn.Name = "arimaBtn";
            this.arimaBtn.Size = new System.Drawing.Size(162, 36);
            this.arimaBtn.TabIndex = 11;
            this.arimaBtn.Text = "SARIMA Forecast with R";
            this.arimaBtn.UseVisualStyleBackColor = true;
            this.arimaBtn.Click += new System.EventHandler(this.arimaBtn_Click);
            // 
            // idSerieTxt
            // 
            this.idSerieTxt.Location = new System.Drawing.Point(92, 282);
            this.idSerieTxt.Name = "idSerieTxt";
            this.idSerieTxt.Size = new System.Drawing.Size(100, 20);
            this.idSerieTxt.TabIndex = 12;
            this.idSerieTxt.Text = "1";
            this.idSerieTxt.TextChanged += new System.EventHandler(this.textBox1_TextChanged_2);
            // 
            // idSerieLbl
            // 
            this.idSerieLbl.AutoSize = true;
            this.idSerieLbl.Location = new System.Drawing.Point(33, 285);
            this.idSerieLbl.Name = "idSerieLbl";
            this.idSerieLbl.Size = new System.Drawing.Size(45, 13);
            this.idSerieLbl.TabIndex = 13;
            this.idSerieLbl.Text = "ID Serie";
            // 
            // tbidcliente
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 393);
            this.Controls.Add(this.idSerieLbl);
            this.Controls.Add(this.idSerieTxt);
            this.Controls.Add(this.arimaBtn);
            this.Controls.Add(this.btnEF);
            this.Controls.Add(this.btnFactory);
            this.Controls.Add(this.tbidclienti);
            this.Controls.Add(this.idclientelbl);
            this.Controls.Add(this.gbServer);
            this.Controls.Add(this.readDotNet);
            this.Controls.Add(this.txtConsole);
            this.Name = "tbidcliente";
            this.Text = "SSD 17/18 - Backend side";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbServer.ResumeLayout(false);
            this.gbServer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtConsole;
        private System.Windows.Forms.Button readDotNet;
        private System.Windows.Forms.RadioButton rbSQLite;
        private System.Windows.Forms.RadioButton rbSQLServer;
        private System.Windows.Forms.GroupBox gbServer;
        private System.Windows.Forms.Label idclientelbl;
        private System.Windows.Forms.TextBox tbidclienti;
        private System.Windows.Forms.Button btnFactory;
        private System.Windows.Forms.Button btnEF;
        private System.Windows.Forms.Button arimaBtn;
        private System.Windows.Forms.TextBox idSerieTxt;
        private System.Windows.Forms.Label idSerieLbl;
    }
}

