use dbTest
GO
IF OBJECT_ID('clienti', 'U') IS NOT NULL DROP TABLE clienti;
CREATE TABLE clienti (
[PK_UID] INT NOT NULL IDENTITY(0,1) PRIMARY KEY,
[id] INTEGER,
[nome] TEXT);
INSERT INTO clienti VALUES(1,'pippo');
INSERT INTO clienti VALUES(2,'pluto');
INSERT INTO clienti VALUES(3,'topolino');
INSERT INTO clienti VALUES(4,'minnie');
IF OBJECT_ID('ordini', 'U') IS NOT NULL DROP TABLE ordini;
CREATE TABLE ordini (
[PK_UID] INT NOT NULL IDENTITY(0,1) PRIMARY KEY,
[id] INTEGER,
[idcliente] INTEGER,
[codice] INTEGER,
[descr] TEXT);
INSERT INTO ordini VALUES(1,1,10,'dieci');
INSERT INTO ordini VALUES(2,1,11,'undici');
INSERT INTO ordini VALUES(3,1,12,'dodici');
INSERT INTO ordini VALUES(4,2,20,'venti');
INSERT INTO ordini VALUES(5,2,21,'ventuno');
INSERT INTO ordini VALUES(6,2,22,'ventidue');
INSERT INTO ordini VALUES(7,3,30,'trenta');
INSERT INTO ordini VALUES(8,3,31,'trentuno');
INSERT INTO ordini VALUES(9,4,40,'quaranta');
INSERT INTO ordini VALUES(10,4,41,'quarantuno');
INSERT INTO ordini VALUES(11,4,42,'quarantadue');
INSERT INTO ordini VALUES(12,4,43,'quarantatre');
INSERT INTO ordini VALUES(13,4,44,'quarantaquattro');